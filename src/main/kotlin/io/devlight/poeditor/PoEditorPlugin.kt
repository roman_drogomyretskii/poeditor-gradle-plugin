package io.devlight.poeditor

import groovy.json.JsonSlurper
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.create
import java.io.File
import java.net.URL


open class PoEditorPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        val extension = project.extensions.create<PoEditorPluginExtension>("PoEditor")
        project.task("downloadStrings") {
            doLast {
                if (checkParams(extension)) {
                    extension.locales!!.forEach {
                        val fileName = it.key
                        val localeCode = it.value

                        val stringsFile = File(extension.pathToModule + fileName)
                            .also {
                                if (it.exists()) {
                                    println("Delete file: $it.absolutePath")
                                    it.delete()
                                }
                            }

                        println("Get language: $localeCode for project: ${extension.projectId}")

                        val process = ProcessBuilder(
                            "curl",
                            "--location",
                            "--request", "POST",
                            "https://api.poeditor.com/v2/projects/export",
                            "--form", "api_token=${extension.apiKey}",
                            "--form", "id=${extension.projectId}",
                            "--form", "language=$localeCode",
                            "--form", "type=android_strings"
                        ).start()

                        process.apply {
                            waitFor()
                            errorStream.reader().use { println(it.readText()) }
                            inputStream.reader().use {
                                val response = it.readText()

                                println(response)

                                val json = JsonSlurper().parseText(response) as Map<*, *>
                                val transFile = (json["result"] as Map<*, *>)["url"] as String

                                println("Download file: $transFile")

                                stringsFile.writeBytes(URL(transFile).readBytes())

                                println(stringsFile.absolutePath)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun checkParams(extension: PoEditorPluginExtension): Boolean {
        val apiKey = extension.apiKey.isNullOrEmpty()
        val projectId = extension.projectId.isNullOrEmpty()
        val pathToModule = extension.pathToModule.isNullOrEmpty()
        val locales = extension.locales.isNullOrEmpty()

        if (apiKey) println("You should declare apiKey")
        if (projectId) println("You should declare projectId")
        if (pathToModule) println("You should declare pathToModule")
        if (locales) println("You should declare locales")

        return !(apiKey and projectId and pathToModule and locales)
    }

}
