package io.devlight.poeditor

open class PoEditorPluginExtension {
    var apiKey: String? = null
    var projectId: String? = null
    var pathToModule: String? = null
    var locales: LinkedHashMap<String, String>? = null
}