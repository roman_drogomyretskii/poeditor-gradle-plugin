plugins {
    id("java-gradle-plugin")
    id("com.gradle.plugin-publish") version "0.10.1"
    id("org.jetbrains.kotlin.jvm") version "1.3.61"
    id("org.gradle.kotlin.kotlin-dsl") version "1.3.3"
}

gradlePlugin {
    plugins {
        create("poEditor"){
            id = "io.devlight.poeditor"
            implementationClass = "io.devlight.poeditor.PoEditorPlugin"
        }
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation ("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
}

val compileKotlin by tasks.getting(org.jetbrains.kotlin.gradle.tasks.KotlinCompile::class) {
    kotlinOptions {
        jvmTarget =  "1.8"
    }
}

pluginBundle {
    // These settings are set for the whole plugin bundle
    website = "http://www.gradle.org/"
    vcsUrl = "https://github.com/gradle/gradle"

    // tags and description can be set for the whole bundle here, but can also
    // be set / overridden in the config for specific plugins
    description = "Plugin for POEditor"

    // The plugins block can contain multiple plugin entries.
    //
    // The name for each plugin block below (greetingsPlugin, goodbyePlugin)
    // does not affect the plugin configuration, but they need to be unique
    // for each plugin.

    // Plugin config blocks can set the id, displayName, version, description
    // and tags for each plugin.

    // id and displayName are mandatory.
    // If no version is set, the project version will be used.
    // If no tags or description are set, the tags or description from the
    // pluginBundle block will be used, but they must be set in one of the
    // two places.

    (plugins) {

        // first plugin
        "poEditor" {
            // id is captured from java-gradle-plugin configuration
            displayName = "poEditor plugin"
            tags = listOf("individual", "tags", "per", "plugin")
            version = "0.1"
        }
    }

    // Optional overrides for Maven coordinates.
    // If you have an existing plugin deployed to Bintray and would like to keep
    // your existing group ID and artifact ID for continuity, you can specify
    // them here.
    //
    // As publishing to a custom group requires manual approval by the Gradle
    // team for security reasons, we recommend not overriding the group ID unless
    // you have an existing group ID that you wish to keep. If not overridden,
    // plugins will be published automatically without a manual approval process.
    //
    // You can also override the version of the deployed artifact here, though it
    // defaults to the project version, which would normally be sufficient.

//    mavenCoordinates {
//        groupId = "org.samples.override"
//        artifactId = "greeting-plugins"
//        version = "1.4"
//    }
}
